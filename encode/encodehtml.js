/**
 * html 特殊字符编码
 * @param s
 * @returns {string}
 */
function htmlEncodeByRegExp (s){

    var REGX_HTML_ENCODE = /"|&|'|<|>|[\x00-\x20]|[\x7F-\xFF]|[\u0100-\u2700]/g;

    return (typeof s != "string") ? s :
        s.replace(REGX_HTML_ENCODE,
            function($0){
                var c = $0.charCodeAt(0), r = ["&#"];
                c = (c == 0x20) ? 0xA0 : c;
                r.push(c); r.push(";");
                return r.join("");
            });
}


/**
 * 数字验证(大于等于0)
 * @param num
 * @returns {boolean}
 */
function checkNum(num){
    var re = /^-?[1-9]+(\.\d+)?$|^-?0(\.\d+)?$|^-?[1-9]+[0-9]*(\.\d+)?$/;
    if (!re.test(num)){
        return false;
    }else{
        if(num<0){
            return false ;
        }else{
            return true ;
        }
    }
}




//判断数字正负
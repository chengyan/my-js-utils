var arrayUtils = {};



/**
 *
 * @desc 判断两个数组是否相等
 * @param {Array} arr1
 * @param {Array} arr2
 * @return {Boolean}
 */
arrayUtils.arrayEqual = function (arr1, arr2) {
    if (arr1 === arr2) return true;
    if (arr1.length != arr2.length) return false;
    for (var i = 0; i < arr1.length; ++i) {
        if (arr1[i] !== arr2[i]) return false;
    }
    return true;
};


/**
 *
 * @desc 求数组最大值
 * @param arr
 * @returns {number}
 */
arrayUtils.getArrayMaxValue = function (arr) {

    return Math.max.apply(null,arr);

};


/**
 * @desc 求数组最大值   用es6方法
 * @param arr
 * @returns {number}
 */
arrayUtils.getArrayMaxValueWithEs6 = function (arr) {

    return Math.max(...arr);
};




/**
 * @desc 求数组最小值
 * @param arr
 * @returns {number}
 */
arrayUtils.getArrayMinValue = function (arr) {

    return Math.min.apply(null,arr);

};



/**
 * @desc  求数组最小值  用es6方法
 * @param arr
 * @returns {number}
 */

arrayUtils.getArrayMinValueWithEs6 = function (arr) {

    return Math.min(...arr);
};


/**
 * 数组去重
 * @returns {Array}
 */
Array.prototype.unique = function () {
    var n = {}, r = []; //n为hash表，r为临时数组
    for (var i = 0; i < this.length; i++) {//遍历当前数组
        if (!n[this[i]]) {//如果hash表中没有当前项
            n[this[i]] = true; //存入hash表
            r.push(this[i]); //把当前数组的当前项push到临时数组里面
        }
    }
    return r;
};




# array #
   ## arrays-utils.js 常用的数组工具 ##
   <li>判断两个数组是否相同</li>
   <li>数组去重</li>
   <li>deconstruction.js   es6 数组解构测试demo</li>
   
   ###confirm
   <li>验证数字的工具，使用场景库房出入库录入时，需要填写金额和数量，此时需要验证数字</li>
   <li>日期验证</li>
   
   ###encode
   <li>编码解码</li>
   
   ###数字工具
   <li>加减乘除四则运算</li>
   <li>判断是否为数字</li>
   
   ###常用正则工具
   <li>日期</li>
   <li>邮箱</li>
   <li>身份证</li>
   <li>手机号</li>
   <li>url地址</li>
